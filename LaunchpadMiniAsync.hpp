#ifndef LAUNCHPADMINIASYNC_HPP
#define LAUNCHPADMINIASYNC_HPP

#include "RtMidi.h"
#include <boost/asio/io_context.hpp>
#include <boost/function.hpp>
#include <memory>
#include "Pad.hpp"
#include "Light.hpp"

namespace novation {

enum : std::uint8_t {
    NOTE_OFF       = 0x80,
    NOTE_ON        = 0x90,
    CONTROL_CHANGE = 0xB0
};

class LaunchpadMiniAsync {
public:
    typedef boost::function<void(Pad pad, std::uint8_t press)> UserCallback_t;
    typedef boost::function<void(RtMidiError::Type type, std::shared_ptr<const std::string> errorText)> MidiErrorCallback_t;

    enum : std::uint8_t {
        ROWS = 9,
        COLS = 9,
    };

private:
    static const std::vector<std::uint8_t> _sysExHeader;
    static std::vector<std::uint8_t> textColorToVector(std::uint32_t colorspec);
    static void midiInCallbackWrapper(double timeStamp, std::vector<std::uint8_t> *message, void *self);
    static void midiInErrorWrapper(RtMidiError::Type type, const std::string &errorText, void *self);
    static void midiOutErrorWrapper(RtMidiError::Type type, const std::string &errorText, void *self);
    static void defaultInErrorCallback(RtMidiError::Type type, std::shared_ptr<const std::string> errorText);
    static void defaultOutErrorCallback(RtMidiError::Type type, std::shared_ptr<const std::string> errorText);
    boost::asio::io_context &_ioc;
    RtMidiIn _in;
    RtMidiOut _out;
    std::uint8_t _padState[ROWS][COLS];
    UserCallback_t _userCallback;
    MidiErrorCallback_t _inErrorCallback;
    MidiErrorCallback_t _outErrorCallback;
    void midiInCallback(double timeStamp, std::shared_ptr<std::vector<std::uint8_t>> message);

public:
    LaunchpadMiniAsync(boost::asio::io_context &ioc, UserCallback_t callback = 0,
                       MidiErrorCallback_t inErrorCallback = defaultInErrorCallback, MidiErrorCallback_t outErrorCallback = defaultOutErrorCallback);

    /**
     * @brief reset
     * Sets the launchpad to programmer mode and disables all text, lighting and powers it on.
     */
    void reset();

    void setCallback(UserCallback_t callback);
    void setInErrorCallback(MidiErrorCallback_t callback);
    void setOutErrorCallback(MidiErrorCallback_t callback);
    void open(unsigned portIn = 2, unsigned portOut = 2);
    void close();

    std::uint8_t getPadState(unsigned x, unsigned y) { return _padState[y][x]; }
    bool getPressedNone();
    bool getPressedAny();
    bool getPressedAll();
    bool getWhitePressedNone();
    bool getWhitePressedAny();
    bool getWhitePressedAll();

    void setSysExProgrammerMode(bool value);

    void setLightSingle(Pad pad, ConstLightPtr light);
    void setLightArray(const ConstLightPtr light[ROWS][COLS]);
    void setLightAll(ConstLightPtr light);

    /**
     * @brief setSysExLightSingle
     * @param pad
     * @param light
     */
    void setSysExLightSingle(Pad pad, ConstLightPtr light);

    /**
     * @brief setSysExLightArray
     * @param light
     */
    void setSysExLightArray(const ConstLightPtr light[ROWS][COLS]);

    // Here, the MSB in color specifies the lighting type, and the other bytes the value
    // Set all pads color
    void setSysExLightAll(ConstLightPtr light);
    /*
    void setSysExLightAllStatic(const std::uint8_t colorPalette);
    void setSysExLightAllFlashing(const std::uint16_t colorBAPalette);
    void setSysExLightAllPulsing(const std::uint8_t colorPalette);
    void setSysExLightAllRGB(const std::uint32_t colorRGB);
    */

    // If the MSB in colorspec is zero, then the LSB indicates the palette color.
    // If it is one, the other three bytes are RGB values.
    void setSysExTextScroll(bool loop, std::uint8_t speed, ConstLightPtr light, const char text[]);
    void setSysExTextScroll(bool loop, std::uint8_t speed, ConstLightPtr light, const std::string &text);

    void setSysExTextScroll(bool loop, std::uint8_t speed, ConstLightPtr light);

    void setSysExTextScroll(bool loop, std::uint8_t speed);

    void setSysExTextScroll(bool loop);

    // Disable text scrolling
    void setSysExTextScrollOff();

    void setSysExPower(bool power);

    // Brightness is from 0 to 127
    void setSysExBrightness(std::uint8_t brightness);
};

}

#endif // LAUNCHPADMINIASYNC_HPP

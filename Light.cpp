#include "Light.hpp"
#include <cassert>

namespace novation {

Light::Light(std::uint8_t type, std::vector<std::uint8_t> data)
    : type(type), data(std::move(data))
{}

std::uint8_t Light::get_type() const {
    return type;
}

std::vector<std::uint8_t> Light::get_data() const {
    return data;
}

LightStatic::LightStatic(std::uint8_t palette)
    : Light(STATIC, {palette})
{}

LightFlashing::LightFlashing(std::uint8_t b, std::uint8_t a)
    : Light(FLASHING, {b, a})
{}

LightPulsing::LightPulsing(std::uint8_t palette)
    : Light(PULSING, {palette})
{}

LightRGB::LightRGB(std::uint8_t r,
                   std::uint8_t g,
                   std::uint8_t b)
    : Light(RGB, {r, g, b})
{}

}
